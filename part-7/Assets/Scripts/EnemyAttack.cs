﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyController))]
public class EnemyAttack : MonoBehaviour
{
    private float timeUntilNextAttack;
    private EnemyController controller;

    [SerializeField]
    private float attackDistance = 1.5f;
    [SerializeField]
    private float attackDuration = 0.3f;
    [SerializeField]
    private float timeBetweenAttacks = 1;
    [SerializeField]
    private int attackDamage = 20;
    [SerializeField]
    private float attackRotation = 45;
    [SerializeField]
    private Transform graphicsTransform;

    private void Start()
    {
        controller = GetComponent<EnemyController>();
    }

    private void Update()
    {
        if (controller.Target == null)
        {
            return;
        }

        timeUntilNextAttack -= Time.deltaTime;
        if (timeUntilNextAttack <= 0)
        {
            float distance = Vector3.Distance(transform.position, controller.Target.position);
            if (distance < attackDistance)
            {
                StartCoroutine(Attack());
                timeUntilNextAttack = timeBetweenAttacks;
            }
        }
    }

    private IEnumerator Attack()
    {
        float attackPercent = 0;
        bool hasAppliedDamage = false;

        Quaternion initialRotation = Quaternion.identity;
        Quaternion targetRotation = Quaternion.Euler(attackRotation, 0, 0);

        while (attackPercent <= 1)
        {
            if (!hasAppliedDamage && attackPercent >= 0.5)
            {
                controller.Target.GetComponent<IDamageable>().TakeDamage(attackDamage);
                hasAppliedDamage = true;
            }
            float t = Mathf.PingPong(attackPercent * 2, 1);
            graphicsTransform.localRotation = Quaternion.Slerp(initialRotation, targetRotation, t);
            attackPercent += Time.deltaTime / attackDuration;
            yield return null;
        }

        graphicsTransform.localRotation = initialRotation;
    }
}
